package version2.test;

import static java.lang.Math.PI;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import version2.src.Cosine;
/**
 * This class contains the test cases which checks for the correctness of Cosine value in First,Second,Third and Fourth Quadrant
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
class TestCosine {
	/**
	 * This function (test case) checks the correctness of Cosine values at (PI/6), (PI/5), (PI/4), (PI/3)
	 */
	@Test
	void checkFirstQuadrant() {
		assertEquals(Cosine.calculateCos(0), Math.cos(0), 1E-14);
		assertEquals(Cosine.calculateCos(PI / 6), Math.cos(PI / 6), 1E-14);
		assertEquals(Cosine.calculateCos(PI / 5), Math.cos(PI / 5), 1E-14);
		assertEquals(Cosine.calculateCos(PI / 4), Math.cos(PI / 4), 1E-14);
		assertEquals(Cosine.calculateCos(PI / 3), Math.cos(PI / 3), 1E-14);
	}
	/**
	 * This function (test case) checks the correctness of Cosine value at (PI/1.5)
	 */
	@Test
	void testSecondQuadrant() {
		assertEquals(Cosine.calculateCos(PI), Math.cos(PI), 1E-15);
		assertEquals(Cosine.calculateCos(PI / 1.5), Math.cos(PI / 1.5), 1E-15);
	}
	/**
	 * This function (test case) checks the correctness of Cosine values at (7*(PI/6)) and (4*(PI/3))
	 */
	@Test
	void testThirdQuadrant() {
		assertEquals(Cosine.calculateCos(7 * PI / 6), Math.cos(7 * PI / 6), 1E-15);
		assertEquals(Cosine.calculateCos(4 * PI / 3), Math.cos(4 * PI / 3), 1E-5);
	}
	/**
	 * This function (test case) checks the correctness of Cosine value at (2*PI) and (5*(PI/3))
	 */
	@Test
	void testFourthQuadrant() {
		assertEquals(Cosine.calculateCos(2 * PI), Math.cos(2 * PI), 1E-5);
		assertEquals(Cosine.calculateCos(5 * PI / 3), Math.cos(5 * PI / 3), 1E-5);
	}

}
