package version2.src;
/**
 * This class contains the implementation of various functions such as fact, powerfunc 
 * which are being used in Taylor series implementation of Sine, Cosine and Tan but it does not contain the absolute function 
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
public class Utilities {
	/**
	 * This method is used for calculating the factorial of passed value 
	 * @param value This parameter receives the value for which the factorial needs to be calculated
	 * @return This returns the factorial value of the passed argument
	 */
	public static double fact(int value) {

		double factorial;
		for (factorial = value; value > 1; value--) {
			factorial = factorial * (value - 1);
		}
		return factorial;
	}
	/**
	 *This method is used for calculating the power  
	 * @param i This is the parameter for which power will be calculated 
	 * @param j This is the parameter which decides the number of times the power will be calculated 
	 * @return This returns the power of the passed argument
	 */
	public static double powerfunc(double i, long j) {
		if (j == 0) {
			return 1;
		}
		for (double k = i; j > 1; j--) {
			i = i * k;
		}
		return i;
	}

}
