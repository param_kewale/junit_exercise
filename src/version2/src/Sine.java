package version2.src;

import static version2.src.Utilities.*;
/**
 * This class contains the Sine Function  
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
public class Sine {
	/**
	 * This function contains the incomplete implementation of Sine function .Loop is not implemented to full extent.
	 * @param value This parameter receives the value for which the Sine Equivalent needs to calculated
	 * @return This return the Sine Value of the parameter received
	 */
	public static double calculateSine(double value) {
		double sum = 0;
        int s = 1;
        for(int counter = 0; counter < 12; counter++)
        {
            sum += s * powerfunc(value,counter * 2 + 1)/fact(counter * 2 + 1);
            s *= -1;
        }
        return sum;
	}
	
}
