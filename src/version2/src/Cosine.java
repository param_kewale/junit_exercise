package version2.src;

import static version2.src.Utilities.*;
/**
 * This class contains the Cosine Function  
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 */
public class Cosine {
	/**
	 * This function contains the incomplete implementation of Cosine function .Loop is not implemented to full extent.
	 * @param value This parameter receives the value for which the Cosine Equivalent needs to calculated
	 * @return This return the Cosine Value of the parameter received
	 */
	public static double calculateCos(double value)
    {
        double result = 1;
        int s = -1;
        for(int counter = 1; counter < 12; counter++) {
            result += s * powerfunc(value,counter*2)/fact(counter*2);
            s *= -1;
        }
        return result;
    }

}
