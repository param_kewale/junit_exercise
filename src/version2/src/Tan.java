package version2.src;
/**
 * This class contains the Tan function
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
public class Tan {
	/**
	 * This function contains the implementation of Tan function but it does not generates any kind of exception for values of tan 90 and tan 270.   
	 * @param value This parameter receives the value for which the Tan Equivalent needs to calculated
	 * @return This return the Tan Value of the parameter received
	 */
	public static double calculateTan(double value)
    {
        return Sine.calculateSine(value)/Cosine.calculateCos(value);
    }
	
}
