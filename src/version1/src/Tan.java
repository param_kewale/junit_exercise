package version1.src;
/**
 * This class contains incomplete implementation of Tan Function  
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
public class Tan {
	/**
	 * This function doesn't contain the Taylor Series Implementation of tan Function.It only return the value which is passed as argument  
	 * @param value This parameter receives the value for which the tan Equivalent needs to calculated
	 * @return This return the tan Value of the parameter received
	 */
	public static double calculateTan(double value)
    {
		return value;
    }
	
}
