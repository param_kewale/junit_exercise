package version1.src;
/**
 * This class contains incomplete implementation of Cosine Function  
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
public class Cosine {
	/**
	 * This function doesn't contain the Taylor Series Implementation of Cosine Function.It only return the value which is passed as argument  
	 * @param value This parameter receives the value for which the Cosine Equivalent needs to calculated
	 * @return This return the Cosine Value of the parameter received
	 */
	public static double calculateCos(double value)
    {
        return value;
    }
	
}
