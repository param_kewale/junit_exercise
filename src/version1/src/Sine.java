package version1.src;
/**
 * This class contains incomplete implementation of Sine Function  
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
public class Sine {
	/**
	 * This function doesn't contain the Taylor Series Implementation of Sine Function.It only return the value which is passed as argument  
	 * @param value This parameter receives the value for which the Sine Equivalent needs to calculated
	 * @return This return the Sine Value of the parameter received
	 */
	public static double calculateSine(double value) {
        return value;
	}
	
}
