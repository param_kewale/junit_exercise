package version1.test;

import static java.lang.Math.PI;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import version1.src.Tan;
/**
 * This class contains the test cases which checks for the correctness of Tan value in First,Second,Third and Fourth Quadrant
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
class TestTan {
	/**
	 * This function (test case) checks the correctness of Tan values at (PI/6), (PI/5), (PI/4), (PI/3) and generates an exception if any error is encountered.
	 */
	@Test
	void checkFirstQuadrant() {
		try {
			assertEquals(Tan.calculateTan(0), Math.tan(0), 1E-14*Math.pow(10,Math.abs((int)Tan.calculateTan(0)/10)+1));
			assertEquals(Tan.calculateTan(PI/6), Math.tan(PI/6), 1E-14*Math.pow(10,Math.abs((int)Tan.calculateTan(PI/6)/10)+1));
			assertEquals(Tan.calculateTan(PI/5), Math.tan(PI/5), 1E-14*Math.pow(10,Math.abs((int)Tan.calculateTan(PI/5)/10)+1));
			assertEquals(Tan.calculateTan(PI/4), Math.tan(PI/4), 1E-14*Math.pow(10,Math.abs((int)Tan.calculateTan(PI/4)/10)+1));
			assertEquals(Tan.calculateTan(PI/3), Math.tan(PI/3), 1E-14*Math.pow(10,Math.abs((int)Tan.calculateTan(PI/3)/10)+1));
			
		} catch(Exception ex) {
			System.out.println("Unexpected Exception");
			ex.printStackTrace();
		}
	}
	/**
	 * This function (test case) checks the correctness of Tan value at (PI),(PI/1.5) and generates an exception if any error is encountered
	 */
	@Test
	void testSecondQuadrant() {
		try {
			assertEquals(Tan.calculateTan(PI), Math.tan(PI), 1E-15*Math.pow(10,Math.abs((int)Tan.calculateTan(PI)/10)+1));
			assertEquals(Tan.calculateTan(PI/1.5), Math.tan(PI/1.5), 1E-15*Math.pow(10,Math.abs((int)Tan.calculateTan(PI/1.5)/10)+1));
		} catch(Exception ex) {
			System.out.println("Unexpected Exception");
			ex.printStackTrace();
		}
	}
	/**
	 * This function (test case) checks the correctness of Tan values at (7*(PI/6)) and (4*(PI/3)) and generates an exception if any error is encountered
	 */
	@Test
	void testThirdQuadrant() {
		try {
			assertEquals(Tan.calculateTan(7*PI/6), Math.tan(7*PI/6), 1E-15*Math.pow(10,Math.abs((int)Tan.calculateTan(7*PI/6)/10)+1));
			assertEquals(Tan.calculateTan(4*PI/3), Math.tan(4*PI/3), 1E-15*Math.pow(10,Math.abs((int)Tan.calculateTan(4*PI/3)/10)+1));
		} catch(Exception ex) {
			System.out.println("Unexpected Exception");
			ex.printStackTrace();
		}
	}
	/**
	 * This function (test case) checks the correctness of Tan values at (2*PI) and (5*(PI/3)) and generates an exception if any error is encountered
	 */
	@Test
	void testFourthQuadrant() {
		try {
			assertEquals(Tan.calculateTan(2*PI), Math.tan(2*PI), 1E-14*Math.pow(10,Math.abs((int)Tan.calculateTan(2*PI)/10)+1));
			assertEquals(Tan.calculateTan(5*PI/3), Math.tan(5*PI/3), 1E-15*Math.pow(10,Math.abs((int)Tan.calculateTan(5*PI/3)/10)+1));
		} catch(Exception ex) {
			System.out.println("Unexpected Exception");
			ex.printStackTrace();
		}
	}

}
