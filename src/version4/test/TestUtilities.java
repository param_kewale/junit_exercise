package version4.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import version4.src.Utilities;
/**
 * This class contains the functions(test cases) for Factorial and Power 
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
class TestUtilities {
	/**
	 * This function tests the value of factorial at 5,4,3,2,1,0.
	 */
	@Test
	void testForFactorial() {
		assertEquals(Utilities.fact(5), 120);
		assertEquals(Utilities.fact(4), 24);
		assertEquals(Utilities.fact(3), 6);
		assertEquals(Utilities.fact(2), 2);
		assertEquals(Utilities.fact(1), 1);
		assertEquals(Utilities.fact(0), 0);
	}
	/**
	 * This function tests the value of 2^2,3^3,6^6,8^8,10^10.
	 */	
	@Test
	void testForPowerFunc() {
		assertEquals(Utilities.powerfunc(2, 2), Math.pow(2, 2));
		assertEquals(Utilities.powerfunc(3, 3), Math.pow(3, 3));
		assertEquals(Utilities.powerfunc(6, 6), Math.pow(6, 6));
		assertEquals(Utilities.powerfunc(8, 8), Math.pow(8, 8));
		assertEquals(Utilities.powerfunc(10, 10), Math.pow(10, 10));
	}
	/**
	 * This function tests the absolute value at -5,10,58,-157,0
	 */
	@Test
	void testForAbsolute() {
		assertEquals(Utilities.absolute(-5), 5);
		assertEquals(Utilities.absolute(10), 10);
		assertEquals(Utilities.absolute(58), 58);
		assertEquals(Utilities.absolute(-157), 157);
		assertEquals(Utilities.absolute(0), 0);
	}

}
