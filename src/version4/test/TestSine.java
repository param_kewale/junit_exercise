package version4.test;

import static java.lang.Math.PI;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import version4.src.Sine;
/**
 * This class contains the test cases which checks for the correctness of Sine value in First,Second,Third and Fourth Quadrant
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
class TestSine {
	/**
	 * This function (test case) checks the correctness of Sine values at (PI/6), (PI/5), (PI/4), (PI/3)
	 */
	@Test
	void checkFirstQuadrant() {
		assertEquals(Sine.calculateSine(0), Math.sin(0), 1E-14);
		assertEquals(Sine.calculateSine(PI / 6), Math.sin(PI / 6), 1E-14);
		assertEquals(Sine.calculateSine(PI / 5), Math.sin(PI / 5), 1E-14);
		assertEquals(Sine.calculateSine(PI / 4), Math.sin(PI / 4), 1E-14);
		assertEquals(Sine.calculateSine(PI / 3), Math.sin(PI / 3), 1E-14);
	}
	/**
	 * This function (test case) checks the correctness of Sine value at (PI/1.5)
	 */
	@Test
	void testSecondQuadrant() {
		assertEquals(Sine.calculateSine(PI), Math.sin(PI), 1E-15);
		assertEquals(Sine.calculateSine(PI / 1.5), Math.sin(PI / 1.5), 1E-15);
	}
	/**
	 * This function (test case) checks the correctness of Sine values at (7*(PI/6)) and (4*(PI/3))
	 */
	@Test
	void testThirdQuadrant() {
		assertEquals(Sine.calculateSine(7 * PI / 6), Math.sin(7 * PI / 6), 1E-15);
		assertEquals(Sine.calculateSine(4 * PI / 3), Math.sin(4 * PI / 3), 1E-15);
	}
	/**
	 * This function (test case) checks the correctness of Sine value at (2*PI) and (5*(PI/3))
	 */
	@Test
	void testFourthQuadrant() {
		assertEquals(Sine.calculateSine(2 * PI), Math.sin(2 * PI), 1E-5);
		assertEquals(Sine.calculateSine(5 * PI / 3), Math.sin(5 * PI / 3), 1E-5);
	}

}
