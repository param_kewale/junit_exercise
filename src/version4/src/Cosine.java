package version4.src;

import static version4.src.Utilities.*;
/**
 *This class contains the Taylor Series Implementation of Cosine Function 
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
public class Cosine {
	/**
	 * This function calculates the Cosine equivalent of the passed value and also has Taylor series implementation
	 * @param radian This parameter receives the value for which the Cosine Equivalent needs to calculated
	 * @return This return the Cosine Value of the parameter received
	 */
	public static double calculateCos(double radian)
    {
		
        double result = 1;
        int s = -1;
        for(int counter = 1; counter < 15; counter++) {
            result += s * powerfunc(radian,counter*2)/fact(counter*2);
            s *= -1;
        }
        return result;
    }
	
	/**
	 * This main method calls the calculateCos function and then prints the value returned by the function 
	 * @param args
	 */
	public static void main(String ...args) {
		System.out.println(Cosine.calculateCos(Math.PI/2.0));
	}
}
