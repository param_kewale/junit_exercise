package version4.src;

import static version4.src.Utilities.*;
/**
 * This class contains the Taylor Series Implementation of Sine Function 
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
public class Sine {
	/**
	 * This function calculates the Sine equivalent of the passed value and also has Taylor series implementation
	 * @param value This parameter receives the value which the Sine equivalent needs to be calculated
	 * @return This returns the sum variable which is the final value obtained after adding all the terms of Taylor Series for the received value
	 */
	public static double calculateSine(double value) {
		double sum = 0;
        int s = 1;
        for(int counter = 0; counter < 20; counter++)
        {
            sum += s * powerfunc(value,counter * 2 + 1)/fact(counter * 2 + 1);
            s *= -1;
        }
        return sum;
	}
	/**
	 * This main method calls the calculateSine function and then prints the value returned by the function
	 * @param args
	 */
	public static void main(String ...args) {
		System.out.println(Sine.calculateSine(Math.PI/2.0));
	}
}
