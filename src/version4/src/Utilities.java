package version4.src;
/**
 * This class contains the implementation of various functions such as fact, powerfunc, 
 * absolute which are being used in Taylor series implementation of Sine, Cosine and Tan
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 * 
 */
public class Utilities {

	/**
	 * This method is used for calculating the factorial of passed value 
	 * @param value This parameter receives the value for which the factorial needs to be calculated
	 * @return This returns the factorial value of the passed argument
	 */
	public static double fact(int value) {

		double factorial = value;
		for (; value > 1; value--) {
			factorial = factorial * (value - 1);
		}
		return factorial;
	}
	/**
	 *This method is used for calculating the power  
	 * @param i This is the parameter for which power will be calculated 
	 * @param j This is the parameter which decides the number of times the power will be calculated 
	 * @return This returns the power of the passed argument
	 */
	public static double powerfunc(double i, long j) {
		if (j == 0) {
			return 1;
		}
		for (double k = i; j > 1; j--) {
			i = i * k;
		}
		return i;
	}
	/**
	 * This method is used for getting the absolute value of any number
	 * @param num This parameter receives the number for which absolute value needs to be calculated
	 * @return This parameter return the absolute value of the passed argument
	 */
	public static double absolute(double num) {
		if (num < 0)
			return -num;
		return num;
	}
}
