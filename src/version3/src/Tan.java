package version3.src;

import static version3.src.Utilities.*;
/**
 * This class contains the Tan Function 
 * @author Damanpal Singh Rayat, Harshit Sahu, Karan Aggarwal, Sarabjit Singh, Param Kewale
 *
 */
public class Tan {
	/**
	 * This function calculates the Tan equivalent of the passed value but it returns the wrong value
	 * @param value This parameter receives the value which Tan equivalent needs to be calculated
	 * @return This return the Tan Value of the parameter received
	 * @throws Exception This method throws an exception if the Input given is tan 90 or 270 degrees 
	 */
	public static double calculateTan(double value)throws Exception
    {
        if(absolute(Cosine.calculateCos(value))<1E-15)
        {
            throw new Exception("Invalid input\nCan not tan 90 degree or 270 degree\nYour degree: "+value+"\ncos: "+Cosine.calculateCos(value));
        }
        return Sine.calculateSine(value)/Cosine.calculateCos(value);
    }
	
}
